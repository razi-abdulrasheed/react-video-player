module.exports = {
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "env": {
    "browser": true,
    "es6": true,
    "node": true,
    "commonjs": true,
    "mongo": true,
    "shared-node-browser": true
  },
  "plugins": [
    "react"
  ],
  "parserOptions": {
    "ecmaVersion": 8,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
      "globalReturn": true,
    }
  },
  "settings": {
    "react": {
      "createClass": "createReactClass", // Regex for Component Factory to use,
      // default to "createReactClass"
      "pragma": "React", // Pragma to use, default to "React"
      "version": "15.0", // React version, default to the latest React stable release
      "flowVersion": "0.53" // Flow version
    },
    "propWrapperFunctions": ["forbidExtraProps"] // The names of any functions used to wrap the
      // propTypes object, e.g. `forbidExtraProps`.
      // If this isn't set, any propTypes wrapped in
      // a function will be skipped.
  },
  "rules": {
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
  }
};